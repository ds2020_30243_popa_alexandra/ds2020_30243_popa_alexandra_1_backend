package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationListItem;
import ro.tuc.ds2020.repositories.MedicationListItemRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.entities.Medication;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationService.class);
    private final MedicationRepository medicationRepository;
    private final MedicationListItemRepository medicationListItemRepository;


    @Autowired
    public MedicationService(MedicationRepository medicationRepository,MedicationListItemRepository medicationListItemRepository) {
        this.medicationRepository = medicationRepository;
        this.medicationListItemRepository=medicationListItemRepository;

    }

    public List<MedicationDTO> findMedications() {
        List<Medication> medicationList = medicationRepository.findAll();
        return medicationList.stream()
                .map(MedicationBuilder::toMedicationDTO)
                .collect(Collectors.toList());
    }

    public MedicationDTO findMedicationById(UUID id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", id);
            throw new ResourceNotFoundException(Medication.class.getSimpleName() + " with id: " + id);
        }
        return MedicationBuilder.toMedicationDTO(prosumerOptional.get());
    }

    public UUID insert(MedicationDTO medicationDTO) {
        Medication medication = MedicationBuilder.toEntity(medicationDTO);
        medication = medicationRepository.save(medication);
        LOGGER.debug("Medication with id {} was inserted in db", medication.getId());
        return medication.getId();
    }

    @Transactional
    public UUID delete(UUID id) {
        Optional<Medication> prosumerOptional = medicationRepository.findById(id);
        Medication medication =prosumerOptional.get();
        if(medication.getMedicationListItem()!=null) {
            for (MedicationListItem med:medication.getMedicationListItem()){
                medicationListItemRepository.delete(med);

            }
        }
        medicationRepository.delete(medication);
        LOGGER.debug("Medication with id {} was deleted in db", medication.getId());
        return medication.getId();
    }

    public MedicationDTO update(MedicationDTO medicationDTO) {
        Medication medication = medicationRepository.findById(medicationDTO.getId()).get();
        if (!medicationRepository.findById(medicationDTO.getId()).isPresent()) {
            LOGGER.error("Medication with id {} was not found in db", medicationDTO.getId());
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + medicationDTO.getId());
        }
        medication.setName(medicationDTO.getName());
        medication.setSideEffects(medicationDTO.getSideEffects());
        medication.setDosage(medicationDTO.getDosage());
        medicationRepository.save(medication);
        return MedicationBuilder.toMedicationDTO(medication);


    }
}
