package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.*;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final UserRepository userRepository;
    private final CaregiverRepository caregiverRepository;
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationListItemRepository medicationListItemRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository,UserRepository userRepository,CaregiverRepository caregiverRepository,MedicationPlanRepository medicationPlanRepository,MedicationListItemRepository medicationListItemRepository) {
        this.patientRepository = patientRepository;
        this.userRepository=userRepository;
        this.caregiverRepository=caregiverRepository;
        this.medicationPlanRepository=medicationPlanRepository;
        this.medicationListItemRepository=medicationListItemRepository;
    }

    public List<PatientDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDTO findPatientById(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDTO(prosumerOptional.get());
    }

    public UUID insert(PatientDetailsDTO patientDTO) {
        Optional<User> prosumerOptional=userRepository.findByUsername(patientDTO.getUsername());

        if(prosumerOptional.isPresent()) {
            LOGGER.error("User existent");
            throw new ResourceNotFoundException(Patient.class.getSimpleName() );

        }
        Patient patient = PatientBuilder.toEntity(patientDTO);
        User user=new User(patientDTO.getUsername(),patientDTO.getPassword());
        user.setRole("patient");
        user.setPatient(patient);
        patient.setUser(user);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }
    @Transactional
    public UUID delete(UUID id) {
        Optional<Patient> prosumerOptional = patientRepository.findById(id);
        Patient patient =prosumerOptional.get();
        if(patient.getCaregiver()!=null){
            patient.getCaregiver().getListOfPatients().remove(patient);
            caregiverRepository.save( patient.getCaregiver());
        }
        if(patient.getMedicationPatientPlan()!=null){
            for (MedicationListItem item:patient.getMedicationPatientPlan().getMedicationListItem()) {
                medicationListItemRepository.delete(item);
            }
            medicationPlanRepository.delete(patient.getMedicationPatientPlan());
        }
        patientRepository.delete(patient);
        userRepository.delete(patient.getUser());
        LOGGER.debug("Patient with id {} was deleted in db", patient.getId());
        return patient.getId();
    }
    @Transactional
    public PatientDTO addCaregiver(PatientDTO patientDTO) {
        Patient patient = patientRepository.findById(patientDTO.getId()).get();
        Caregiver caregiver=caregiverRepository.findByName(patientDTO.getCaregiver()).get();
        System.out.println(caregiver.getListOfPatients().size());
        if(patient.getCaregiver()!=null) {
            patient.getCaregiver().deletePatient(patient);
            caregiverRepository.save(patient.getCaregiver());
        }

        patient.setCaregiver(caregiver);
        caregiver.addPatient(patient);
        patientRepository.save(patient);
        caregiverRepository.save(caregiver);
        return PatientBuilder.toPatientDTO(patient);

    }

    public PatientDTO update(PatientDTO patientDTO) {
        Patient patient = patientRepository.findById(patientDTO.getId()).get();
        if (!patientRepository.findById(patientDTO.getId()).isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientDTO.getId());
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientDTO.getId());
        }
        if(patientDTO.getCaregiver()!=null)
        {
            Caregiver caregiver=caregiverRepository.findByName(patientDTO.getCaregiver()).get();
            if(patient.getCaregiver()!=null) {

                patient.getCaregiver().getListOfPatients().remove(patient);
                caregiverRepository.save(patient.getCaregiver());
            }
            patient.setCaregiver(caregiver);
            caregiver.addPatient(patient);
            caregiverRepository.save(caregiver);
        }

        patient.setName(patientDTO.getName());
        patient.setAddress(patientDTO.getAddress());
        patient.setBirthDate(patientDTO.getBirthDate());
        patient.setGender(patientDTO.getGender());
        patient.setMedicalRecord(patientDTO.getMedicalRecord());
        patientRepository.save(patient);
        return PatientBuilder.toPatientDTO(patient);

    }


    public List<PatientDTO> findPatientsByCaregiverId(UUID caregiverID) {
        Caregiver caregiver=caregiverRepository.findById(caregiverID).get();

        return caregiver.getListOfPatients().stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());

    }
}
