package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.dtos.builders.DoctorBuilder;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.UserRepository;
import ro.tuc.ds2020.entities.Doctor;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class DoctorService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DoctorService.class);
    private final DoctorRepository doctorRepository;
    private final UserRepository userRepository;

    @Autowired
    public DoctorService(DoctorRepository doctorRepository,UserRepository userRepository) {
        this.doctorRepository = doctorRepository;
        this.userRepository=userRepository;
    }

    public List<DoctorDTO> findDoctors() {
        List<Doctor> doctorList = doctorRepository.findAll();
        return doctorList.stream()
                .map(DoctorBuilder::toDoctorDTO)
                .collect(Collectors.toList());
    }

    public DoctorDTO findDoctorById(UUID id) {
        Optional<Doctor> prosumerOptional = doctorRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", id);
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + id);
        }
        return DoctorBuilder.toDoctorDTO(prosumerOptional.get());
    }

    public UUID insert(DoctorDTO doctorDTO) {
        Optional<User> prosumerOptional=userRepository.findByUsername(doctorDTO.getUsername());

        if(prosumerOptional.isPresent()) {
            LOGGER.error("User existent");
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() );

        }
        Doctor doctor = DoctorBuilder.toEntity(doctorDTO);
        User user=new User(doctorDTO.getUsername(),doctorDTO.getPassword());
        user.setRole("doctor");
        user.setDoctor(doctor);
        doctor.setUser(user);
        doctor = doctorRepository.save(doctor);
        LOGGER.debug("Doctor with id {} was inserted in db", doctor.getId());
        return doctor.getId();
    }

    public UUID delete(UUID id) {
        Optional<Doctor> prosumerOptional = doctorRepository.findById(id);
        Doctor doctor =prosumerOptional.get();
        doctorRepository.delete(doctor);
        userRepository.delete(doctor.getUser());
        LOGGER.debug("Doctor with id {} was deleted in db", doctor.getId());
        return doctor.getId();
    }

    public DoctorDTO update(DoctorDTO doctorDTO) {
        Doctor doctor = doctorRepository.findById(doctorDTO.getId()).get();
        if (!doctorRepository.findById(doctorDTO.getId()).isPresent()) {
            LOGGER.error("Doctor with id {} was not found in db", doctorDTO.getId());
            throw new ResourceNotFoundException(Doctor.class.getSimpleName() + " with id: " + doctorDTO.getId());
        }
        doctor.setName(doctorDTO.getName());

        doctorRepository.save(doctor);
        return DoctorBuilder.toDoctorDTO(doctor);

    }

}
