package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.dtos.MedicationListItemDTO;
import ro.tuc.ds2020.dtos.builders.MedicationListItemBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.repositories.MedicationListItemRepository;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.MedicationRepository;
import ro.tuc.ds2020.entities.MedicationListItem;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;


@Service
public class MedicationListItemService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationListItemService.class);
    private final MedicationListItemRepository medicationListItemRepository;
    private final MedicationRepository medicationRepository;
    private final MedicationPlanRepository medicationPlanRepository;


    @Autowired
    public MedicationListItemService(MedicationListItemRepository medicationListItemRepository,MedicationRepository medicationRepository,MedicationPlanRepository medicationPlanRepository) {
        this.medicationListItemRepository = medicationListItemRepository;
        this.medicationRepository=medicationRepository;
        this.medicationPlanRepository=medicationPlanRepository;

    }

    @Transactional
    public UUID insert(MedicationListItemDTO medicationListItemDTO) {
        //System.out.println(medicationListItemDTO.getMedication());
        MedicationListItem medicationListItem = MedicationListItemBuilder.toEntity(medicationListItemDTO);
        Medication medication = medicationRepository.findById(medicationListItemDTO.getMedication()).get();
        MedicationPlan medicationPlan = medicationPlanRepository.findById(medicationListItemDTO.getMedicationPlan()).get();
        medicationPlan.addMedicationListItem(medicationListItem);
        medication.addMedicationListItem(medicationListItem);
        medicationListItem.setMedication(medication);
        medicationListItem.setMedicationPlan(medicationPlan);
        medicationListItem = medicationListItemRepository.save(medicationListItem);
        LOGGER.debug("MedicationListItem with id {} was inserted in db", medicationListItem.getId());
        return medicationListItem.getId();
    }

    public List<MedicationListItemDTO> findMedicationListItem() {

        List<MedicationListItem> medicationListItem = medicationListItemRepository.findAll();
        return medicationListItem.stream()
                .map(MedicationListItemBuilder::toMedicationListItemDTO)
                .collect(Collectors.toList());
    }
    public UUID delete(UUID id) {
        Optional<MedicationListItem> prosumerOptional = medicationListItemRepository.findById(id);
        MedicationListItem medicationListItem =prosumerOptional.get();
        medicationListItemRepository.delete(medicationListItem);
        LOGGER.debug("MedicationListItem with id {} was deleted in db", medicationListItem.getId());
        return medicationListItem.getId();
    }

}
