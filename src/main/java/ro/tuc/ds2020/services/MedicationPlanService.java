package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.*;
import ro.tuc.ds2020.repositories.DoctorRepository;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MedicationPlanService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MedicationPlanService.class);
    private final MedicationPlanRepository medicationPlanRepository;
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;


    @Autowired
    public MedicationPlanService(MedicationPlanRepository medicationPlanRepository,PatientRepository patientRepository,DoctorRepository doctorRepository) {
        this.medicationPlanRepository = medicationPlanRepository;
        this.patientRepository=patientRepository;
        this.doctorRepository=doctorRepository;

    }

    public List<MedicationPlanDTO> findMedicationPlan() {
        List<MedicationPlan> medicationList = medicationPlanRepository.findAll();
        return medicationList.stream()
                .map(MedicationPlanBuilder::toMedicationPlanDTO)
                .collect(Collectors.toList());
    }

    @Transactional
   public UUID insert(MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan medicationPlan = MedicationPlanBuilder.toEntity(medicationPlanDTO);
        Patient patient=patientRepository.findById(medicationPlanDTO.getPatientUser()).get();
        Doctor doctor=doctorRepository.findById(medicationPlanDTO.getDoctorUser()).get();
        medicationPlan.setPatientUser(patient);
        patient.setMedicationPatientPlan(medicationPlan);
        doctor.addMedicationPlan(medicationPlan);
        medicationPlan.setDoctorUser(doctor);
        medicationPlan = medicationPlanRepository.save(medicationPlan);
        LOGGER.debug("MedicationPlan with id {} was inserted in db", medicationPlan.getId());
        return medicationPlan.getId();
    }

    @Transactional
    public MedicationPlanDetailsDTO findMedicationPlanByPatientId(UUID patientID) {
        Patient patient=patientRepository.findById(patientID).get();
        return MedicationPlanBuilder.toMedicationPlanDetailsDTO(patient.getMedicationPatientPlan());
    }

}
