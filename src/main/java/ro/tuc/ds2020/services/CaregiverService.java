package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.dtos.builders.CaregiverBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.User;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.PatientRepository;
import ro.tuc.ds2020.repositories.UserRepository;
import ro.tuc.ds2020.entities.Caregiver;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CaregiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CaregiverService.class);
    private final CaregiverRepository caregiverRepository;
    private final UserRepository userRepository;
    private final PatientRepository patientRepository;


    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository,UserRepository userRepository,PatientRepository patientRepository) {
        this.caregiverRepository = caregiverRepository;
        this.userRepository= userRepository;
        this.patientRepository=patientRepository;
    }

    public List<CaregiverDTO> findCaregivers() {
        List<Caregiver> caregiverList = caregiverRepository.findAll();
        return caregiverList.stream()
                .map(CaregiverBuilder::toCaregiverDTO)
                .collect(Collectors.toList());
    }

    public CaregiverDTO findCaregiverById(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", id);
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + id);
        }
        System.out.println(prosumerOptional.get().getListOfPatients().size());
        return CaregiverBuilder.toCaregiverDTO(prosumerOptional.get());
    }

    public UUID insert(CaregiverDetailsDTO caregiverDTO) {
        Optional<User> prosumerOptional=userRepository.findByUsername(caregiverDTO.getUsername());

        if(prosumerOptional.isPresent()) {
            LOGGER.error("User existent");
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() );

        }
        Caregiver caregiver = CaregiverBuilder.toEntity(caregiverDTO);
        User user = new User(caregiverDTO.getUsername(), caregiverDTO.getPassword());
        user.setRole("caregiver");
        user.setCaregiver(caregiver);
        caregiver.setUser(user);
        caregiver = caregiverRepository.save(caregiver);
        LOGGER.debug("Caregiver with id {} was inserted in db", caregiver.getId());
        return caregiver.getId();
    }

    public UUID delete(UUID id) {
        Optional<Caregiver> prosumerOptional = caregiverRepository.findById(id);
        Caregiver caregiver =prosumerOptional.get();
        for (Patient p:caregiver.getListOfPatients()) {
            p.setCaregiver(null);
            patientRepository.save(p);
        }
        caregiver.deleteAllPatients();
        caregiverRepository.delete(caregiver);
        userRepository.delete(caregiver.getUser());
        LOGGER.debug("Caregiver with id {} was deleted in db", caregiver.getId());
        return caregiver.getId();

    }

    public CaregiverDTO update(CaregiverDTO caregiverDTO) {
        Caregiver caregiver = caregiverRepository.findById(caregiverDTO.getId()).get();
        if (!caregiverRepository.findById(caregiverDTO.getId()).isPresent()) {
            LOGGER.error("Caregiver with id {} was not found in db", caregiverDTO.getId());
            throw new ResourceNotFoundException(Caregiver.class.getSimpleName() + " with id: " + caregiverDTO.getId());
        }
        caregiver.setName(caregiverDTO.getName());
        caregiver.setBirthDate(caregiverDTO.getBirthDate());
        caregiver.setGender(caregiverDTO.getGender());
        caregiver.setAddress(caregiverDTO.getAddress());
        caregiverRepository.save(caregiver);
        return CaregiverBuilder.toCaregiverDTO(caregiver);


    }

}
