package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired ;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.dtos.builders.UserBuilder;
import ro.tuc.ds2020.entities.User;

import ro.tuc.ds2020.repositories.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UserService {

   private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
   private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserDTO> findUsers() {
        List<User> patientList = userRepository.findAll();
        return patientList.stream()
                .map(UserBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public UserDetailsDTO logIn(UserDTO user) {
        Optional<User> prosumerOptional = userRepository.findByUsername(user.getUsername());
        String role="";
        UUID id=null;
        if (prosumerOptional.isPresent()  && prosumerOptional.get().getPassword().equals(user.getPassword())) {
                User u = prosumerOptional.get();

                switch (u.getRole()) {
                    case "patient":
                        role = "patient";
                        id = u.getPatient().getId();
                        break;
                    case "caregiver":
                        role = "caregiver";
                        id = u.getCaregiver().getId();
                        break;
                    case "doctor":
                        role = "doctor";
                        id = u.getDoctor().getId();
                        break;
                    default:break;
                }
        }
        else{
            LOGGER.error("User with username {} or password was not found in db", user.getUsername());
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + user.getId());
        }

        return UserBuilder.toUserDetailsDTO(id,role);
    }


    public UserDTO update(UserDTO userDTO) {
        User user = userRepository.findById(userDTO.getId()).get();
        if (!userRepository.findById(userDTO.getId()).isPresent()) {
            LOGGER.error("User with id {} was not found in db", userDTO.getId());
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + userDTO.getId());
        }
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassword());
        userRepository.save(user);
        return UserBuilder.toUserDTO(user);

    }

    public UUID delete(UUID userID) {
        Optional<User> prosumerOptional = userRepository.findById(userID);
        if (!userRepository.findById(userID).isPresent()) {
            LOGGER.error("User with id {} was not found in db", userID);
            throw new ResourceNotFoundException(User.class.getSimpleName() + " with id: " + userID);
        }
        User user =prosumerOptional.get();
        userRepository.delete(user);

        LOGGER.debug("User with id {} was deleted in db", user.getId());
        return user.getId();
    }
}
