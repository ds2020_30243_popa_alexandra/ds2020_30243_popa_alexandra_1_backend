package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="MedicationPlan")
public class MedicationPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "patientUserID", referencedColumnName = "id")
    private Patient patientUser;

    @ManyToOne
    @JoinColumn(name="doctorUserID", nullable=false)
    private Doctor doctorUser;

    @Column(name = "periodOfTheTreatment",nullable = false)
    private String  periodOfTheTreatment;

    @OneToMany(mappedBy="medicationPlan")
    private List<MedicationListItem> medicationListItem;



    public MedicationPlan() {
    }

    public MedicationPlan( String periodOfTheTreatment) {
        this.periodOfTheTreatment = periodOfTheTreatment;
        this.medicationListItem = new ArrayList<MedicationListItem>();
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Patient getPatientUser() {
        return patientUser;
    }

    public void setPatientUser(Patient patientUser) {
        this.patientUser = patientUser;
    }

    public Doctor getDoctorUser() {
        return doctorUser;
    }

    public void setDoctorUser(Doctor doctorUser) {
        this.doctorUser = doctorUser;
    }

    public String getPeriodOfTheTreatment() {
        return periodOfTheTreatment;
    }

    public void setPeriodOfTheTreatment(String periodOfTheTreatment) {
        this.periodOfTheTreatment = periodOfTheTreatment;
    }

    public List<MedicationListItem> getMedicationListItem() {
        return medicationListItem;
    }

    public void setMedicationListItem(List<MedicationListItem> medicationListItem) {
        this.medicationListItem = medicationListItem;
    }
    public void addMedicationListItem(MedicationListItem item) {
        this.medicationListItem.add(item);
    }

}


