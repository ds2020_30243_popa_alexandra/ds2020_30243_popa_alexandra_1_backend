package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "doctors")
public class Doctor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToMany(mappedBy="doctorUser")
    private List<MedicationPlan> medicationDoctorPlans;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userID")
    private User user;

    public Doctor() {
    }

    public Doctor(String name) {
        this.name = name;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MedicationPlan> getMedicationDoctorPlans() {
        return medicationDoctorPlans;
    }

    public void setMedicationDoctorPlans(List<MedicationPlan> medicationDoctorPlans) {
        this.medicationDoctorPlans = medicationDoctorPlans;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    public void addMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationDoctorPlans.add(medicationPlan);
    }
}