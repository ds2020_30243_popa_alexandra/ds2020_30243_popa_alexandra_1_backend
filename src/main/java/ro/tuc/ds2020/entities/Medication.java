package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="Medication")
public class Medication implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "sideEffects", nullable = false)
    private String sideEffects;

    @Column(name = "dosage", nullable = false)
    private String dosage ;

    @OneToMany(mappedBy = "medication")
    private List<MedicationListItem> medicationListItem;

    public Medication() {
    }

    public Medication(String name, String sideEffects,String dosage ) {
        this.name = name;
        this.sideEffects=sideEffects;
        this.dosage=dosage;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSideEffects() {
        return sideEffects;
    }

    public void setSideEffects(String sideEffects) {
        this.sideEffects = sideEffects;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public List<MedicationListItem> getMedicationListItem() {
        return medicationListItem;
    }

    public void setMedicationListItem(List<MedicationListItem> medicationListItem) {
        this.medicationListItem = medicationListItem;
    }

    public void addMedicationListItem(MedicationListItem medicationListItem) {
        this.medicationListItem.add(medicationListItem);
    }
}

