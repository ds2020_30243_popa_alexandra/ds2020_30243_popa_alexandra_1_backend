package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.User;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository extends CrudRepository<User,Long>,JpaRepository<User,Long>,PagingAndSortingRepository<User,Long>,JpaSpecificationExecutor<User>{

    Optional<User> findById(UUID id);

    Optional<User> findByUsername(String username);
}