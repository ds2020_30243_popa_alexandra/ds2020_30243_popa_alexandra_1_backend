package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.entities.Medication;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MedicationRepository extends CrudRepository<Medication,Long>,JpaRepository<Medication,Long>,PagingAndSortingRepository<Medication,Long>,JpaSpecificationExecutor<Medication>{

    Optional<Medication> findById(UUID id);
}