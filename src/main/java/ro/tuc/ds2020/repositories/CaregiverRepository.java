package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.entities.Caregiver;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@Repository
public interface CaregiverRepository extends CrudRepository<Caregiver,Long>,JpaRepository<Caregiver,Long>,PagingAndSortingRepository<Caregiver,Long>,JpaSpecificationExecutor<Caregiver>{

    Optional<Caregiver> findById(UUID id);


    Optional<Caregiver> findByName(String caregiver);
}