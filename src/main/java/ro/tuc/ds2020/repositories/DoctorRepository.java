package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.entities.Doctor;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface DoctorRepository extends CrudRepository<Doctor,Long>,JpaRepository<Doctor,Long>,PagingAndSortingRepository<Doctor,Long>,JpaSpecificationExecutor<Doctor>{

    Optional<Doctor> findById(UUID id);
}