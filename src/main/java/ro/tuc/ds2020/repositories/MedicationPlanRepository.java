package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MedicationPlanRepository extends CrudRepository<MedicationPlan,Long>,JpaRepository<MedicationPlan,Long>,PagingAndSortingRepository<MedicationPlan,Long>,JpaSpecificationExecutor<MedicationPlan>{
    Optional<MedicationPlan> findById(UUID id);
}