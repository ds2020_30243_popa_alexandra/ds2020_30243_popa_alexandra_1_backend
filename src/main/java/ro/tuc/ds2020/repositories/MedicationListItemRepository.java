package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.dtos.MedicationListItemDTO;
import ro.tuc.ds2020.entities.MedicationListItem;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MedicationListItemRepository extends CrudRepository<MedicationListItem,Long>,JpaRepository<MedicationListItem,Long>,PagingAndSortingRepository<MedicationListItem,Long>,JpaSpecificationExecutor<MedicationListItem>{
    Optional<MedicationListItem> findById(UUID id);

    //List<MedicationListItemDTO> findByAll();
    //Optional<MedicationListItem> findById(UUID id);
}