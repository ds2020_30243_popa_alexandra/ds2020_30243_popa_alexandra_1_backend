package ro.tuc.ds2020.repositories;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import ro.tuc.ds2020.entities.Patient;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface PatientRepository extends CrudRepository<Patient,Long>,JpaRepository<Patient,Long>,PagingAndSortingRepository<Patient,Long>,JpaSpecificationExecutor<Patient>{

    Optional<Patient> findById(UUID id);

}