package ro.tuc.ds2020.dtos;


import java.util.UUID;

public class MedicationListItemDetailsDTO {
    private  UUID id;
    private  String  intakeIntervals;
    private MedicationDTO medication;

    public MedicationListItemDetailsDTO(UUID id, String intakeIntervals, MedicationDTO medication) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.medication = medication;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }
}
