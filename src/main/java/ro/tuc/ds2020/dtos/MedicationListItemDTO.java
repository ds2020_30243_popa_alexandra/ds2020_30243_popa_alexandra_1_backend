package ro.tuc.ds2020.dtos;


import java.util.UUID;

public class MedicationListItemDTO {

    private UUID id;
    private  String  intakeIntervals;
    private  UUID  medication;
    private  UUID  medicationPlan;

    public MedicationListItemDTO(UUID id, String intakeIntervals,UUID medication,UUID  medicationPlan) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.medication=medication;
        this.medicationPlan=medicationPlan;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public UUID getMedication() {
        return medication;
    }

    public void setMedication(UUID medication) {
        this.medication = medication;
    }

    public UUID getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(UUID medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
