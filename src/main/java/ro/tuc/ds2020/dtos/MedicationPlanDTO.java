package ro.tuc.ds2020.dtos;

import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;
    private UUID patientUser;
    private UUID doctorUser;
    private String  periodOfTheTreatment;


    public MedicationPlanDTO(UUID id, UUID patientUser, UUID doctorUser, String periodOfTheTreatment) {
        this.id = id;
        this.patientUser = patientUser;
        this.doctorUser = doctorUser;
        this.periodOfTheTreatment = periodOfTheTreatment;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getPatientUser() {
        return patientUser;
    }

    public void setPatientUser(UUID patientUser) {
        this.patientUser = patientUser;
    }

    public UUID getDoctorUser() {
        return doctorUser;
    }

    public void setDoctorUser(UUID doctorUser) {
        this.doctorUser = doctorUser;
    }

    public String getPeriodOfTheTreatment() {
        return periodOfTheTreatment;
    }

    public void setPeriodOfTheTreatment(String periodOfTheTreatment) {
        this.periodOfTheTreatment = periodOfTheTreatment;
    }

}
