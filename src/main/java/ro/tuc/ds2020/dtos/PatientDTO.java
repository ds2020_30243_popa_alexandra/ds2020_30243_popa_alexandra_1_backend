package ro.tuc.ds2020.dtos;


import java.sql.Date;
import java.util.UUID;

public class PatientDTO {

    private UUID id;
    private String name;
    private Date birthDate;
    private String gender;
    private String address;
    private String medicalRecord;
    private String caregiver;

    public PatientDTO(UUID id, String name, Date birthDate, String gender, String address, String medicalRecord, String caregiver) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.gender = gender;
        this.address = address;
        this.medicalRecord = medicalRecord;
        this.caregiver = caregiver;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    /*public MedicationPlan getMedicationPatientPlan() {
        return medicationPatientPlan;
    }

    public void setMedicationPatientPlan(MedicationPlan medicationPatientPlan) {
        this.medicationPatientPlan = medicationPatientPlan;
    }*/

    public String getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(String caregiver) {
        this.caregiver = caregiver;
    }
}