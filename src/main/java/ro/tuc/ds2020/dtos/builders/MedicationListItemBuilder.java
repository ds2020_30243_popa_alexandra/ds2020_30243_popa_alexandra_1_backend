package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.MedicationListItem;

import static ro.tuc.ds2020.dtos.builders.MedicationBuilder.toMedicationDTO;

public class MedicationListItemBuilder {
    public MedicationListItemBuilder() {
    }
    public static MedicationListItemDTO toMedicationListItemDTO(MedicationListItem medicationListItem) {
        return new MedicationListItemDTO(medicationListItem.getId(),
                medicationListItem.getIntakeIntervals(),
                medicationListItem.getMedication().getId(),
                medicationListItem.getMedicationPlan().getId());
    }
    public static MedicationListItemDetailsDTO toMedicationListItemDetailsDTO(MedicationListItem medicationListItem) {

        return new MedicationListItemDetailsDTO(medicationListItem.getId(),
                medicationListItem.getIntakeIntervals(),
                toMedicationDTO(medicationListItem.getMedication()));
    }

    public static MedicationListItem toEntity(MedicationListItemDTO medicationListItemDTO) {
        return new MedicationListItem(medicationListItemDTO.getIntakeIntervals());
    }
}
