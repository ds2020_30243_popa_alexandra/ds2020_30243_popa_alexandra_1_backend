package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {
    private CaregiverBuilder() {
    }
    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(),caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress()/*, caregiver.getListOfPatients()*/);
    }

    public static CaregiverDetailsDTO toCaregiverDetailsDTO(Caregiver caregiver) {
        return new CaregiverDetailsDTO(caregiver.getId(), caregiver.getName(),caregiver.getBirthDate(), caregiver.getGender(), caregiver.getAddress(), caregiver.getUser().getUsername(), caregiver.getUser().getPassword());
    }


    public static Caregiver toEntity(CaregiverDetailsDTO caregiverDetailsDTO) {
        return new Caregiver(caregiverDetailsDTO.getName(),
                caregiverDetailsDTO.getBirthDate(),
                caregiverDetailsDTO.getGender(),
                caregiverDetailsDTO.getAddress());
    }
}
