package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;

import ro.tuc.ds2020.entities.MedicationPlan;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MedicationPlanBuilder {
    public MedicationPlanBuilder() {
    }
    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getPatientUser().getId(),medicationPlan.getDoctorUser().getId(),medicationPlan.getPeriodOfTheTreatment());
    }

    public static MedicationPlanDetailsDTO toMedicationPlanDetailsDTO(MedicationPlan medicationPlan) {
        List<MedicationListItemDetailsDTO> list=new ArrayList<MedicationListItemDetailsDTO>( medicationPlan.getMedicationListItem().stream()
                .map(MedicationListItemBuilder::toMedicationListItemDetailsDTO)
                .collect(Collectors.toList()));

        return new MedicationPlanDetailsDTO(medicationPlan.getId(),medicationPlan.getPeriodOfTheTreatment(),list);
    }
    public static MedicationPlan toEntity(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(medicationPlanDTO.getPeriodOfTheTreatment());
    }
}
