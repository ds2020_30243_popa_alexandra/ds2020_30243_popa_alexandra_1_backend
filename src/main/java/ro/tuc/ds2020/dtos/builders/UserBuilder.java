package ro.tuc.ds2020.dtos.builders;


import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.entities.User;

import java.util.UUID;

public class UserBuilder {


    private UserBuilder() {
    }

    public static UserDTO toUserDTO(User user) {
        return new UserDTO(user.getId(), user.getUsername(), user.getPassword());
    }
    public static UserDetailsDTO toUserDetailsDTO(UUID id,String role) {
        return new UserDetailsDTO(id,role);
    }

    public static User toEntity(UserDTO userDTO) {
        return new User(userDTO.getUsername(),userDTO.getPassword());
    }
}
