package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.entities.*;



public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        String caregiverName;
        if(patient.getCaregiver()==null) caregiverName=null;
        else caregiverName=patient.getCaregiver().getName();
        return new PatientDTO(patient.getId(), patient.getName(),patient.getBirthDate(), patient.getGender(), patient.getAddress(), patient.getMedicalRecord(),caregiverName);
    }

    public static PatientDetailsDTO toPatientDetailsDTO(Patient patient) {
        return new PatientDetailsDTO(patient.getId(), patient.getName(),patient.getBirthDate(), patient.getGender(), patient.getAddress(),patient.getMedicalRecord(), patient.getUser().getUsername(),patient.getUser().getPassword());
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Patient(patientDetailsDTO.getName(),
                patientDetailsDTO.getBirthDate(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getAddress(),
                patientDetailsDTO.getMedicalRecord());

    }
}
