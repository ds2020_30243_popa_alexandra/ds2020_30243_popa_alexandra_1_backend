package ro.tuc.ds2020.dtos;



import java.util.List;
import java.util.UUID;

public class MedicationPlanDetailsDTO {
    private UUID id;
    private String  periodOfTheTreatment;
    private List<MedicationListItemDetailsDTO> items;

    public MedicationPlanDetailsDTO(UUID id, String periodOfTheTreatment, List<MedicationListItemDetailsDTO> items) {
        this.id = id;
        this.periodOfTheTreatment = periodOfTheTreatment;
        this.items = items;
    }

    public String getPeriodOfTheTreatment() {
        return periodOfTheTreatment;
    }

    public void setPeriodOfTheTreatment(String periodOfTheTreatment) {
        this.periodOfTheTreatment = periodOfTheTreatment;
    }

    public List<MedicationListItemDetailsDTO> getItems() {
        return items;
    }

    public void setItems(List<MedicationListItemDetailsDTO> items) {
        this.items = items;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
