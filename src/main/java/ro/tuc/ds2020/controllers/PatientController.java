package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;



@RestController
@CrossOrigin
@RequestMapping("/patient")
public class PatientController {

    final
    PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

     @GetMapping
     public ResponseEntity<List<PatientDTO>> getPatients() {
         List<PatientDTO> dtos = patientService.findPatients();

         return new ResponseEntity<>(dtos, HttpStatus.OK);
     }
     @PostMapping
     public ResponseEntity<UUID> insertPatient(@Valid @RequestBody PatientDetailsDTO patient) {
         UUID patientID = patientService.insert(patient);
         return new ResponseEntity<>(patientID, HttpStatus.CREATED);
     }
     @PutMapping
     public  ResponseEntity<PatientDTO> update(@Valid @RequestBody PatientDTO patient){
        PatientDTO dto=patientService.update(patient);
         return new ResponseEntity<>(dto, HttpStatus.OK);
     }

    @GetMapping("/{id}")
    public  ResponseEntity<PatientDTO> getPatientById(@PathVariable("id") UUID patientID){
        PatientDTO dto=patientService.findPatientById(patientID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

     @DeleteMapping("/{id}")
     public ResponseEntity<UUID> delete(@PathVariable("id") UUID patientID){
         UUID id = patientService.delete(patientID);
         return new ResponseEntity<>(id,HttpStatus.OK);
     }

}
