package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.services.MedicationPlanService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/medicationPlan")
public class MedicationPlanController {

    final
    MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

     @GetMapping
     public ResponseEntity<List<MedicationPlanDTO>> getMedicationPlan() {
         List<MedicationPlanDTO> dtos = medicationPlanService.findMedicationPlan();

         return new ResponseEntity<>(dtos, HttpStatus.OK);
     }
    @GetMapping("/{id}")
    public  ResponseEntity<MedicationPlanDetailsDTO> getMedicationPlanByPatientId(@PathVariable("id") UUID patientID){
        MedicationPlanDetailsDTO dto=medicationPlanService.findMedicationPlanByPatientId(patientID);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<UUID> insertMedicationPlan(@Valid @RequestBody MedicationPlanDTO medication) {
        UUID medicationID = medicationPlanService.insert(medication);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }

}
