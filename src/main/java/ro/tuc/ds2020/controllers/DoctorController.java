package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.DoctorDTO;
import ro.tuc.ds2020.services.DoctorService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/doctor")
public class DoctorController {

    final
    DoctorService doctorService;

    @Autowired
    public DoctorController(DoctorService doctorService) {
        this.doctorService = doctorService;
    }

    @GetMapping
    public ResponseEntity<List<DoctorDTO>> getDoctors() {
        List<DoctorDTO> dtos = doctorService.findDoctors();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<UUID> insertDoctor(@Valid @RequestBody DoctorDTO doctor) {
        UUID patientID = doctorService.insert(doctor);
        return new ResponseEntity<>(patientID, HttpStatus.CREATED);
    }
    @PutMapping
    public  ResponseEntity<DoctorDTO> update(@Valid @RequestBody DoctorDTO doctor){
        DoctorDTO dto=doctorService.update(doctor);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<UUID> delete(@PathVariable("id") UUID doctorID){
        UUID id = doctorService.delete(doctorID);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }

}
