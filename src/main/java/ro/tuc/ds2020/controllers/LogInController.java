package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.UserDTO;
import ro.tuc.ds2020.dtos.UserDetailsDTO;
import ro.tuc.ds2020.services.UserService;

import javax.validation.Valid;


@RestController
@CrossOrigin
@RequestMapping("/login")
public class LogInController {

    final
    UserService userService;

    @Autowired
    public LogInController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<UserDetailsDTO> logIn(@Valid @RequestBody UserDTO user) {
        UserDetailsDTO userDetails = userService.logIn(user);
        return new ResponseEntity<>(userDetails, HttpStatus.CREATED);
    }


}
