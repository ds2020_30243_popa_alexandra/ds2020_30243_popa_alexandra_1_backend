package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.*;
import ro.tuc.ds2020.services.CaregiverService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/caregiver")
public class CaregiverController {

    final
    CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping
    public ResponseEntity<List<CaregiverDTO>> getCaregivers() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiverById(@PathVariable("id") UUID caregiverID) {
        CaregiverDTO dtos = caregiverService.findCaregiverById(caregiverID);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDetailsDTO caregiver) {
        UUID caregiverID = caregiverService.insert(caregiver);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }
    @PutMapping
    public  ResponseEntity<CaregiverDTO> update(@Valid @RequestBody CaregiverDTO caregiver){
        CaregiverDTO dto=caregiverService.update(caregiver);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<UUID> delete(@PathVariable("id") UUID caregiverID){
        UUID id = caregiverService.delete(caregiverID);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }
}
