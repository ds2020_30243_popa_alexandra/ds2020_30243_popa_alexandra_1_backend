package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/medication")
public class MedicationController {

    final
    MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping
    public ResponseEntity<List<MedicationDTO>> getMedications() {
        List<MedicationDTO> dtos = medicationService.findMedications();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<UUID> insertMedication(@Valid @RequestBody MedicationDTO medication) {
        UUID medicationID = medicationService.insert(medication);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }
    @PutMapping
    public  ResponseEntity<MedicationDTO> update(@Valid @RequestBody MedicationDTO medication){
        MedicationDTO dto=medicationService.update(medication);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<UUID> delete(@PathVariable("id") UUID userID){
        UUID id = medicationService.delete(userID);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }
}
