package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.MedicationListItemDTO;
import ro.tuc.ds2020.services.MedicationListItemService;


import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/medicationListItem")
public class MedicationListItemController {

    final
    MedicationListItemService medicationListItemService;

    @Autowired
    public MedicationListItemController(MedicationListItemService medicationListItemService) {
        this.medicationListItemService = medicationListItemService;
    }

    @GetMapping
    public ResponseEntity<List<MedicationListItemDTO>> getMedicationListItem() {
        List<MedicationListItemDTO> dtos = medicationListItemService.findMedicationListItem();

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }
    @PostMapping
    public ResponseEntity<UUID> insertMedicationListItem(@Valid @RequestBody MedicationListItemDTO medication) {
        UUID medicationID = medicationListItemService.insert(medication);
        return new ResponseEntity<>(medicationID, HttpStatus.CREATED);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<UUID> delete(@PathVariable("id") UUID medicationListItemID){
        UUID id = medicationListItemService.delete(medicationListItemID);
        return new ResponseEntity<>(id,HttpStatus.OK);
    }
}
