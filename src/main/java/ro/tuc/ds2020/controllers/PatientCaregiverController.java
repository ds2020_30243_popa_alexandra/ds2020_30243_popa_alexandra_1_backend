package ro.tuc.ds2020.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;



@RestController
@CrossOrigin
@RequestMapping("/patient_caregiver")
public class PatientCaregiverController {

    final
    PatientService patientService;

    @Autowired
    public PatientCaregiverController(PatientService patientService) {
        this.patientService = patientService;
    }


    @PutMapping
    public  ResponseEntity<PatientDTO> setCaregiver(@Valid @RequestBody PatientDTO patient){
        PatientDTO dto=patientService.addCaregiver(patient);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<List<PatientDTO>> getPatientsByCaregiverId(@PathVariable("id") UUID caregiverID) {
        List<PatientDTO> dtos = patientService.findPatientsByCaregiverId(caregiverID);

        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


}
